using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Camera : MonoBehaviour
{
    public Vector3 CameraOffset;
    public float OffsetSpeed;
    GameStates GS;

    // Start is called before the first frame update
    void Start()
    {
        GS = GameObject.Find("Scripts").GetComponent<GameStates>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GS.CurrentPlayersTurn.ToString()=="Player_1")
            transform.position = Vector3.Lerp(transform.position, GameObject.FindWithTag("Player").transform.position + CameraOffset, Time.deltaTime * OffsetSpeed);
        if (GS.CurrentPlayersTurn.ToString()=="Player_2")
            transform.position = Vector3.Lerp(transform.position, GameObject.FindWithTag("Player_2").transform.position + CameraOffset, Time.deltaTime * OffsetSpeed);
        if (GS.CurrentPlayersTurn.ToString()=="Player_3")
            transform.position = Vector3.Lerp(transform.position, GameObject.FindWithTag("Player_3").transform.position + CameraOffset, Time.deltaTime * OffsetSpeed);
        if (GS.CurrentPlayersTurn.ToString()=="Player_4")
            transform.position = Vector3.Lerp(transform.position, GameObject.FindWithTag("Player_4").transform.position + CameraOffset, Time.deltaTime * OffsetSpeed);
    }
}
