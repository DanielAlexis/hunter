﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour
{
    public float PaddingAmount = 1.1f;
    public GameObject Tile1;
    public GameObject[] Players;

    int[, ] Map1 = new int[, ]
    {
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0},
        {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0},
    };
 
    // Start is called before the first frame update
    void Start()
    {
        Setup(Map1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void GenerateBoard(int[, ] Map)
    {
        for(int row=0; row < Map1.GetLength(0); row++)
        {
            for(int col=0; col < Map1.GetLength(1); col++)
            {
                switch(Map[row, col])
                {
                    case 1:
                        Instantiate(Tile1, new Vector3(row * PaddingAmount, 0, col * PaddingAmount), Quaternion.identity);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void GeneratePlayers(int[, ] Map)
    {
        bool IsPlayerPlaced = false;

        foreach (GameObject PlayerPrefab in Players)
        {
            while(IsPlayerPlaced==false)
            {           
                int RandX = Random.Range(0, Map.GetLength(0));
                int RandY = Random.Range(0, Map.GetLength(1));

                if (Map[RandX, RandY] == 1)
                {
                    Instantiate(PlayerPrefab, new Vector3(RandX * PaddingAmount, 0.5f, RandY * PaddingAmount), Quaternion.identity);
                    IsPlayerPlaced = true;
                }
            }
            IsPlayerPlaced=false;
        }
    }

    public void Setup(int[, ] Map)
    {
        GenerateBoard(Map);
        GeneratePlayers(Map);
    }
}
