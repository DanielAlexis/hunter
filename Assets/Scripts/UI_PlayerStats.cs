using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerStats : MonoBehaviour
{
    public GenerateMap GM;

    // Start is called before the first frame update
    void Start()
    {
        GM = GetComponent<GenerateMap>();
        
        UpdateAllUIStats();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateAllUIStats()
    {
        // PLAYER 1
        GameObject.Find("/Canvas/Player Panel/Value_NAME").GetComponent<Text>().text = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().NAME;
        GameObject.Find("/Canvas/Player Panel/Value_HP").GetComponent<Text>().text = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().HP.ToString();
        GameObject.Find("/Canvas/Player Panel/Value_STR").GetComponent<Text>().text = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().STR.ToString();
        GameObject.Find("/Canvas/Player Panel/Value_DEF").GetComponent<Text>().text = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().DEF.ToString();
        GameObject.Find("/Canvas/Player Panel/Value_MOV").GetComponent<Text>().text = GameObject.FindWithTag("Player").GetComponent<PlayerStats>().MOV.ToString();

        // PLAYER 2
        GameObject.Find("/Canvas/Player_2 Panel/Value_NAME").GetComponent<Text>().text = GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().NAME;
        GameObject.Find("/Canvas/Player_2 Panel/Value_HP").GetComponent<Text>().text = GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().HP.ToString();
        GameObject.Find("/Canvas/Player_2 Panel/Value_STR").GetComponent<Text>().text = GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().STR.ToString();
        GameObject.Find("/Canvas/Player_2 Panel/Value_DEF").GetComponent<Text>().text = GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().DEF.ToString();
        GameObject.Find("/Canvas/Player_2 Panel/Value_MOV").GetComponent<Text>().text = GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().MOV.ToString();

        // PLAYER 3
        GameObject.Find("/Canvas/Player_3 Panel/Value_NAME").GetComponent<Text>().text = GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().NAME;
        GameObject.Find("/Canvas/Player_3 Panel/Value_HP").GetComponent<Text>().text = GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().HP.ToString();
        GameObject.Find("/Canvas/Player_3 Panel/Value_STR").GetComponent<Text>().text = GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().STR.ToString();
        GameObject.Find("/Canvas/Player_3 Panel/Value_DEF").GetComponent<Text>().text = GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().DEF.ToString();
        GameObject.Find("/Canvas/Player_3 Panel/Value_MOV").GetComponent<Text>().text = GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().MOV.ToString();

        // PLAYER 4
        GameObject.Find("/Canvas/Player_4 Panel/Value_NAME").GetComponent<Text>().text = GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().NAME;
        GameObject.Find("/Canvas/Player_4 Panel/Value_HP").GetComponent<Text>().text = GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().HP.ToString();
        GameObject.Find("/Canvas/Player_4 Panel/Value_STR").GetComponent<Text>().text = GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().STR.ToString();
        GameObject.Find("/Canvas/Player_4 Panel/Value_DEF").GetComponent<Text>().text = GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().DEF.ToString();
        GameObject.Find("/Canvas/Player_4 Panel/Value_MOV").GetComponent<Text>().text = GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().MOV.ToString();
    }
}
