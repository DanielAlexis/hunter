using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStates : MonoBehaviour
{
    public enum GameState
    {
        Idle,
        Moving,
        Attacking,
    }

    public enum PlayerTurnState
    {
        Player_1,
        Player_2,
        Player_3,
        Player_4,
    }

    public GameState CurrentState;
    public PlayerTurnState CurrentPlayersTurn;

    void Start()
    {
        CurrentState=GameState.Idle;
        CurrentPlayersTurn=PlayerTurnState.Player_1;

        GameObject.Find("/Canvas/Text_CurrentGameStateValue").GetComponent<Text>().text=CurrentState.ToString();
        GameObject.Find("/Canvas/Text_CurrentPlayerTurnValue").GetComponent<Text>().text=CurrentPlayersTurn.ToString();
    }

    void Update()
    {
        
    }

    public void Button_Move()
    {
        UpdateGameState(GameState.Moving);
    }

    public void Button_Attack()
    {
        UpdateGameState(GameState.Attacking);
    }

    public void Button_Debug()
    {
        if (CurrentPlayersTurn==PlayerTurnState.Player_1)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_2);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_2)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_3);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_3)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_4);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_4)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_1);
        }
    }

    public void UpdateGameState(GameState State)
    {
        CurrentState=State;
        GameObject.Find("/Canvas/Text_CurrentGameStateValue").GetComponent<Text>().text=CurrentState.ToString();
    }

    public void UpdatePlayerTurns(PlayerTurnState State)
    {
        CurrentPlayersTurn=State;
        GameObject.Find("/Canvas/Text_CurrentPlayerTurnValue").GetComponent<Text>().text=CurrentPlayersTurn.ToString();
    }

    public void EndTurn()
    {
        if (CurrentPlayersTurn==PlayerTurnState.Player_1)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_2);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_2)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_3);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_3)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_4);
        }
        else if (CurrentPlayersTurn==PlayerTurnState.Player_4)
        {
            UpdatePlayerTurns(PlayerTurnState.Player_1);
        }
    }
}
