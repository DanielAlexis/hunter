using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Player : MonoBehaviour
{
    GameStates GS;
    GenerateMap GM;
    public GameObject FocusedPlayer;
    public Material CanMoveMaterial;

    // Start is called before the first frame update
    void Start()
    {
        GS = transform.GetComponent<GameStates>();
        GM = transform.GetComponent<GenerateMap>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GS.CurrentState!=GameStates.GameState.Moving)
        DeMovifyAllTiles();
    }

    public void MovePlayer()
    {
        if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_1)
            FocusedPlayer=GameObject.FindWithTag("Player");
        if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_2)
            FocusedPlayer=GameObject.FindWithTag("Player_2");
        if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_3)
            FocusedPlayer=GameObject.FindWithTag("Player_3");
        if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_4)
            FocusedPlayer=GameObject.FindWithTag("Player_4");

        foreach(GameObject tile in GM.PrefabList)
        {
            TileInfo tempTile = tile.GetComponent<TileInfo>();
            PlayerStats tempStats = FocusedPlayer.GetComponent<PlayerStats>();

            if (((tempStats.PlayerPosition.x-tempTile.Location.x) + (tempStats.PlayerPosition.y-tempTile.Location.y)) <= FocusedPlayer.GetComponent<PlayerStats>().MOV &&  
                ((tempStats.PlayerPosition.x-tempTile.Location.x) + (tempStats.PlayerPosition.y-tempTile.Location.y)) >= -FocusedPlayer.GetComponent<PlayerStats>().MOV &&
                ((tempStats.PlayerPosition.x-tempTile.Location.x) - (tempStats.PlayerPosition.y-tempTile.Location.y)) <= FocusedPlayer.GetComponent<PlayerStats>().MOV &&
                ((tempStats.PlayerPosition.x-tempTile.Location.x) - (tempStats.PlayerPosition.y-tempTile.Location.y)) >= -FocusedPlayer.GetComponent<PlayerStats>().MOV)
            {
                foreach(GameObject player in GM.PlayerList)
                {
                    if (tempTile.Location!=player.GetComponent<PlayerStats>().PlayerPosition)
                    {
                        tile.GetComponent<TileInfo>().CanMove = true;
                        tile.GetComponent<Renderer>().material = CanMoveMaterial;
                    }
                }
            }
            else
            {
                tile.GetComponent<TileInfo>().CanMove = false;
                tile.GetComponent<Renderer>().material = tile.GetComponent<TileInfo>().RememberedMaterial;
            }
        }
    }

    public void DeMovifyAllTiles()
    {
        foreach(GameObject tile in GM.PrefabList)
        {
            tile.GetComponent<TileInfo>().CanMove = false;
            tile.GetComponent<Renderer>().material = tile.GetComponent<TileInfo>().RememberedMaterial;
        }
    }

    public void MovePlayerHere()
    {
        
    }
}
