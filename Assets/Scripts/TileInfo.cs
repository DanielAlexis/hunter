﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileInfo : MonoBehaviour
{
    public Material RememberedMaterial {get; set; }
    public Vector2 Location;
    public bool CanMove = false;
    public GameStates GS;
    public Movement_Player MP;

    // Start is called before the first frame update
    void Start()
    {
        RememberedMaterial=GetComponent<Renderer>().material;
        GS = GameObject.Find("Scripts").GetComponent<GameStates>();
        MP = GameObject.Find("Scripts").GetComponent<Movement_Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseOver(){
        if(Input.GetMouseButtonDown(0) && CanMove==true)
        {
            if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_1)
            {
                GameObject.FindWithTag("Player").transform.position = new Vector3(this.transform.position.x, GameObject.FindWithTag("Player").transform.position.y, this.transform.position.z);
                GameObject.FindWithTag("Player").GetComponent<PlayerStats>().PlayerPosition = Location;
            }
            else if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_2)
            {
                GameObject.FindWithTag("Player_2").transform.position = new Vector3(this.transform.position.x, GameObject.FindWithTag("Player_2").transform.position.y, this.transform.position.z);
                GameObject.FindWithTag("Player_2").GetComponent<PlayerStats>().PlayerPosition = Location;
            }
            else if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_3)
            {
                GameObject.FindWithTag("Player_3").transform.position = new Vector3(this.transform.position.x, GameObject.FindWithTag("Player_3").transform.position.y, this.transform.position.z);
                GameObject.FindWithTag("Player_3").GetComponent<PlayerStats>().PlayerPosition = Location;
            }
            else if (GS.CurrentPlayersTurn==GameStates.PlayerTurnState.Player_4)
            {
                GameObject.FindWithTag("Player_4").transform.position = new Vector3(this.transform.position.x, GameObject.FindWithTag("Player_4").transform.position.y, this.transform.position.z);
                GameObject.FindWithTag("Player_4").GetComponent<PlayerStats>().PlayerPosition = Location;
            }
            
            MP.DeMovifyAllTiles();
            GS.EndTurn();
        }
    }
}
