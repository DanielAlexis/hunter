﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMap : MonoBehaviour
{
    public float PaddingAmount = 1.1f;
    public GameObject Tile1;
    public GameObject[] Players;

    public int[, ] Map1 = new int[, ]
    {
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0},
        {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0},
    };

    public List<GameObject> PrefabList;
    public List<GameObject> PlayerList; // Dupe map above, so I can reference tiles properly.
 
    // Start is called before the first frame update
    void Start()
    {
        Setup(Map1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void GenerateBoard(int[, ] Map)
    {
        for(int row=0; row < Map.GetLength(0); row++)
        {
            for(int col=0; col < Map.GetLength(1); col++)
            {
                switch(Map[row, col])
                {
                    case 1:
                        GameObject temp = Instantiate(Tile1, new Vector3(row * PaddingAmount, 0, col * PaddingAmount), Quaternion.identity);
                        TileInfo script = temp.GetComponent<TileInfo>();
                        script.Location = new Vector2(row, col);
                        PrefabList.Add(temp);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void GeneratePlayers(int[, ] Map)
    {
        foreach (GameObject PlayerPrefab in Players)
        {
            bool IsPlaced=false;

            while(IsPlaced==false)
            {
                int RandX = Random.Range(0, Map.GetLength(0));
                int RandY = Random.Range(0, Map.GetLength(1));

                if (Map[RandX, RandY] == 1)
                {
                    GameObject temp = Instantiate(PlayerPrefab, new Vector3(RandX * PaddingAmount, 0.5f, RandY * PaddingAmount), Quaternion.identity);
                    temp.GetComponent<PlayerStats>().PlayerPosition = new Vector2(RandX, RandY);
                    PlayerList.Add(temp);
                    IsPlaced=true;
                }
            }
        }
    }

    public void Setup(int[, ] Map)
    {
        GenerateBoard(Map);
        GeneratePlayers(Map);
    }
}
